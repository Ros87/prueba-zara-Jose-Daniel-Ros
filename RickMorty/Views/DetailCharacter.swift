//
//  DetailCharacter.swift
//  RickMorty
//
//  Created by Ros on 7/11/23.
//

import SwiftUI
import CachedAsyncImage

struct DetailCharacter: View {
    var character: Characters
    
    var body: some View {
        Color("BackgroudApp")
            .edgesIgnoringSafeArea(.all)
            .overlay(
                ZStack{
                    Divider()
                        .background(Color.clear)
                    
                    VStack (alignment: .leading) {
                        CachedAsyncImage(url: URL(string: character.image)) { img in
                            img
                                .resizable()
                                .cornerRadius(20)
                                .fixedSize()
                        } placeholder: {
                            ProgressView()
                        }
                        Text(character.name)
                            .font(.title)
                            .fontWeight(.bold)
                            .foregroundStyle(Color.white)
                        
                        HStack {
                            Circle()
                                .foregroundColor(character.getStatusColor())
                                .frame(width: 16, height: 16)
                            Text(character.status)
                                .font(.title3)
                                .foregroundStyle(Color.white)
                            Rectangle()
                                .frame(width: 10, height: 2)
                                .foregroundColor(.white)
                            Text(character.species)
                                .font(.title3)
                                .foregroundStyle(Color.white)
                        }
                        
                        Text("Last know location:")
                            .padding(.top, 20)
                            .font(.title3)
                            .foregroundStyle(Color.gray)
                        Text(character.location.name)
                            .font(.title2)
                            .foregroundStyle(Color.white)
                        
                        Text("First seen in:")
                            .padding(.top, 20)
                            .font(.title3)
                            .foregroundStyle(Color.gray)
                        Text(character.episode[0])
                            .font(.title2)
                            .foregroundStyle(Color.white)
                        Spacer()
                    }
                    
                    .navigationBarTitleDisplayMode(.inline)
                    .padding()
                    .background(Color("BackgroundSecondary"))
                    /*.edgesIgnoringSafeArea(.all)*/
                }
                    .padding([.top, .bottom], 6.0)
            )
    }
}

#Preview {
    DetailCharacter(character: Characters(id: 1, name: "Evil Morty", status: "Alive", species: "Human", type: "", gender: "Male", origin: Origin(name: "unknown", url: ""), location: Origin(name: "Citadel of Ricks", url: "https://rickandmortyapi.com/api/location/3"), image: "https://rickandmortyapi.com/api/character/avatar/118.jpeg", episode: ["https://rickandmortyapi.com/api/episode/10","https://rickandmortyapi.com/api/episode/28","https://rickandmortyapi.com/api/episode/51"], url: "https://rickandmortyapi.com/api/character/118", created: "2017-12-26T16:13:41.103Z"))
}
