//
//  RowView.swift
//  RickMorty
//
//  Created by Ros on 6/11/23.
//

import SwiftUI
import CachedAsyncImage

struct RowView: View {
    //@Environment(\.modelContext) var modelContext
    
    var character: Characters
    var body: some View {
        HStack {
            CachedAsyncImage(url: URL(string: character.image)) { img in
                img
                    .resizable()
                    .cornerRadius(20)
            } placeholder: {
                ProgressView()
            }
            .frame(width: 60, height: 60)
            Text(character.name)
                .font(.title)
                .fontWeight(.bold)
                .foregroundStyle(Color.white)
            //Text("id: \(character.id)")
        }
        .background(Color.clear)
    }
}

#Preview {
    RowView(character: Characters(id: 1, name: "Evil Morty", status: "Alive", species: "Human", type: "", gender: "Male", origin: Origin(name: "unknown", url: ""), location: Origin(name: "Citadel of Ricks", url: "https://rickandmortyapi.com/api/location/3"), image: "https://rickandmortyapi.com/api/character/avatar/118.jpeg", episode: ["https://rickandmortyapi.com/api/episode/10","https://rickandmortyapi.com/api/episode/28","https://rickandmortyapi.com/api/episode/51"], url: "https://rickandmortyapi.com/api/character/118", created: "2017-12-26T16:13:41.103Z"))
}
