//
//  ListView.swift
//  RickMorty
//
//  Created by Ros on 6/11/23.
//

import SwiftUI
import SwiftData

struct ListView: View {
   // @Environment(\.modelContext) var modelContext
 //   private var viewModel: ViewModel = ViewModel()
 //   @Query() var characters: [Characters]
    @Environment(ViewModel.self) var viewModel
   // @State var nextIndex = 2
    @State private var searchText = ""
    @State private var hasAppeared = false
    
    var body: some View {
        NavigationView {
            VStack(spacing: 0){
                Divider()
                    .background(Color.clear)
                List (viewModel.characters) { character in
                    NavigationLink(destination: DetailCharacter(character: character)) {
                        RowView(character: character)
                            .onAppear {
                                if viewModel.shouldLoadData(id: character.id) {
                                    viewModel.getCharacters()
                                    
                                }
                            }
                    }
                    .tint(Color.green)
                    .background(Color.clear)
                    .listRowBackground(Color("BackgroundSecondary"))
                }
                /* .background(Color.yellow)
                 .scrollContentBackground(.hidden)*/
                .searchable(text: $searchText, prompt: Text("Search name"))
                
                .onChange(of: searchText) {
                    viewModel.searchCharacters(name: searchText)
                }
                .onAppear{
                    if !hasAppeared {
                        hasAppeared = true
                        viewModel.getCharacters()
                    }
                    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = .white
                    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .black
                }
            }
            .navigationTitle("Characters")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Image("Rick_and_Morty")
                        .resizable()
                        .scaledToFit()
                }
            }
            .background(Color("BackgroudApp").ignoresSafeArea(.all))
            .scrollContentBackground(.hidden)
        }
    }
    
}

#Preview {
    ListView()
}
