//
//  RickMortyApp.swift
//  RickMorty
//
//  Created by Ros on 6/11/23.
//

import SwiftUI
import SwiftData

@main
struct RickMortyApp: App {
/*      var sharedModelContainer: ModelContainer = {
     let schema = Schema([
     Location.self,
     Characters.self,
     Origin.self
     ])
     let modelConfiguration = ModelConfiguration(schema: schema, isStoredInMemoryOnly: true)
     
     do {
     return try ModelContainer(for: schema, configurations: [modelConfiguration])
     } catch {
     fatalError("Could not create ModelContainer: \(error)")
     }
     }()
     
     var body: some Scene {
     WindowGroup {
     //ContentView()
     ListView()
     }
     .modelContainer(sharedModelContainer)
     }
     }*/
    
    
   var body: some Scene {
        WindowGroup {
            ListView()
                .environmentObject(ViewModel())
        }
       
    }
}
