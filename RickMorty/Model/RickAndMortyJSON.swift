//
//  RickAndMortyJSON.swift
//  RickMorty
//
//  Created by Ros on 6/11/23.
//

import Foundation

struct RickAndMortyJSON: Decodable {
    let info: Info
    let results: [Characters]
}

struct Info: Decodable {
    let count: Int
    let pages: Int
    let next: String?
    let prev: String?
}

