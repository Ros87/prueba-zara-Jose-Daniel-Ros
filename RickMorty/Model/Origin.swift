//
//  Origin.swift
//  RickMorty
//
//  Created by Ros on 6/11/23.
//

import Foundation
import SwiftData

@Model
class Origin: Codable {
    enum CodingKeys: CodingKey {
        case name, url
    }
    
    var name: String
    @Attribute(.unique) var url: String
    
    init(name: String, url: String) {
        self.name = name
        self.url = url
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.url = try container.decode(String.self, forKey: .url)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(url, forKey: .url)
    }
}
