//
//  Characters.swift
//  RickMorty
//
//  Created by Ros on 6/11/23.
//

import Foundation
import SwiftData
import SwiftUI

@Model
class Characters: Codable {
    
    enum CodingKeys: CodingKey {
        case id, name, status, species, type, gender, origin, location, image, episode, url, created
    }
    
    @Attribute(.unique) var id: Int
    var name: String
    var status: String
    var species: String
    var type: String
    var gender: String
    var origin: Origin
    var location: Origin
    var image: String
    var episode: [String]
    var url: String
    var created: String
    
    init(id: Int, name: String, status: String, species: String, type: String, gender: String, origin: Origin, location: Origin, image: String, episode: [String], url: String, created: String) {
        self.id = id
        self.name = name
        self.status = status
        self.species = species
        self.type = type
        self.gender = gender
        self.origin = origin
        self.location = location
        self.image = image
        self.episode = episode
        self.url = url
        self.created = created
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.status = try container.decode(String.self, forKey: .status)
        self.species = try container.decode(String.self, forKey: .species)
        self.type = try container.decode(String.self, forKey: .type)
        self.gender = try container.decode(String.self, forKey: .gender)
        self.origin = try container.decode(Origin.self, forKey: .origin)
        self.location = try container.decode(Origin.self, forKey: .location)
        self.image = try container.decode(String.self, forKey: .image)
        self.episode = try container.decode([String].self, forKey: .episode)
        self.url = try container.decode(String.self, forKey: .url)
        self.created = try container.decode(String.self, forKey: .created)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(status, forKey: .status)
        try container.encode(species, forKey: .species)
        try container.encode(type, forKey: .type)
        try container.encode(gender, forKey: .gender)
        try container.encode(origin, forKey: .origin)
        try container.encode(location, forKey: .location)
        try container.encode(image, forKey: .image)
        try container.encode(episode, forKey: .episode)
        try container.encode(url, forKey: .url)
        try container.encode(created, forKey: .created)
        
    }
    
    func getStatusColor() -> Color {
        switch self.status {
        case "Alive":
            return .green
        case "Dead":
            return .red
        default :
            return .gray
        }
    }
}
