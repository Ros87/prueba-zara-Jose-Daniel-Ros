//
//  ViewModel.swift
//  RickMorty
//
//  Created by Ros on 6/11/23.
//

import Foundation
import SwiftData

@Observable
final class ViewModel: ObservableObject {
    let urlCharacters = "https://rickandmortyapi.com/api/character"
    let urlLocations = "https://rickandmortyapi.com/api/location"
    let urlEpisodes = "https://rickandmortyapi.com/api/episode"
    
  //  @Environment(\.modelContext) var modelContext
    var container: ModelContainer = {
        let schema = Schema([
            Location.self,
            Characters.self
        ])
        let modelConfiguration = ModelConfiguration(schema: schema, isStoredInMemoryOnly: true)

        do {
            return try ModelContainer(for: schema, configurations: [modelConfiguration])
        } catch {
            fatalError("Could not create ModelContainer: \(error)")
        }
    }()
   
    @MainActor
    var modelContext: ModelContext {
        container.mainContext
    }
    
    var characters: [Characters] = []
    var next: String? = ""
    var stringSearch: String? = ""
    
    func getCharacters(_ index: Int) {
        let url : URL?
        if index == 1 {
            url = URL(string: urlCharacters)
        } else {
            url = URL(string: "\(urlCharacters)/?page=\(index)")
        }
        
        downloadCharacters(url)
        
    }
    
    @MainActor
    func searchCharacters (name: String) {
        next = ""
        stringSearch = name
        deleteAllCharacters()
        getCharacters()
    }
    
    @MainActor
    func getCharacters() {
       //getCharacters(1)
        let url : URL?
        
        if let stringUrl = next, !stringUrl.isEmpty {
            url = URL(string: stringUrl)
        } else if let searchString = stringSearch, !searchString.isEmpty {
            url = URL(string: "\(urlCharacters)/?name=\(searchString)")
        } else {
            deleteAllCharacters()
            url = URL(string: urlCharacters)
        }
        
        
        downloadCharacters(url)
    }
    
    private func downloadCharacters (_ url: URL?) {
        let urlSession = URLSession.shared
        urlSession.dataTask(with: url!) { data, response, error in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any]
               // print(json)
                do {
                    let decoder = JSONDecoder()
                    let dataJson = try decoder.decode(RickAndMortyJSON.self, from: data)
                    self.next = dataJson.info.next
                    print("valores de la busqueda")
                    print(dataJson.info.count)
                    
                    for result in dataJson.results {
                      /*  let characterJSON = try? JSONSerialization.data(withJSONObject: result)
                      //  print("tenemos el characterJSON -> \(characterJSON)")
                        let character = try decoder.decode(Characters.self, from: characterJSON!)*/
                        self.modelContext.insert(result)
                      //  print(character)
                    }
                  //  self.characters = []
                    self.obtainCharacters()
                    
               /*   //  print("sacamos el json array")
                    if let results = json?["results"] as? [[String: Any]] {
                       // print("tenemos el json array")
                        let decoder = JSONDecoder()
                     //   print("generamos el decoder")
                        for result in results {
                          //  print("result -> \(result)")
                            let characterJSON = try? JSONSerialization.data(withJSONObject: result)
                          //  print("tenemos el characterJSON -> \(characterJSON)")
                            let character = try decoder.decode(Characters.self, from: characterJSON!)
                            self.modelContext.insert(character)
                          //  print(character)
                        }
                      //  self.characters = []
                        self.obtainCharacters()
                    }*/
                } catch {
                    print("Error: \(error)")
                }
                
            }
            
        }.resume()
    }
    
    func shouldLoadData(id: Int) -> Bool {
        if characters.count < 20 {
            return false
        }
        return id == characters[characters.count - 2].id
    }
    
    @MainActor
    private func obtainCharacters(){
        let fetchDescriptor = FetchDescriptor<Characters> (predicate: nil, sortBy: [SortDescriptor<Characters>(\.id)])
        
        characters = try! modelContext.fetch(fetchDescriptor)
        print("nuemro de entradas -> \(characters.count)")
    }
    
    @MainActor
    private func deleteAllCharacters() {
        characters.forEach {
            modelContext.delete($0)
        }
        characters = []
     //   obtainCharacters()
    }
}
